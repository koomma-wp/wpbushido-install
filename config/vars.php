<?php
/**
 * Created by PhpStorm.
 * User: Philippe AUTIER
 * Date: 04/01/19
 * Time: 15:04
 */

return array (
    'ENVIRONEMENT' => $_ENV["STACK_ENVIRONMENT"],
    'DEV_MAIL' => 'tech@leoo.fr',
    'WEB_DOMAIN' => $_ENV["MAIN_APP_NAME"].'-local.leoo-factory.io',
);