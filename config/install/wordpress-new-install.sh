#!/bin/bash
echo "\e[36mStart Wordpress BUSHIDO Install\e[0m";
while true; do
    read -p "Do you wish to perform a fresh install ? [y or n] " yn
    case $yn in
        [Yy]* )
        echo "[\e[92mINSTALL\e[0m] - Start Wordpress Environment BUSHIDO\e[0m";
        mv wordpress/web/wp-content wordpress/web/wp-sites
        echo "[\e[92mINSTALL\e[0m] - Wp Content to Wp Sites done !\e[0m";
        cp wordpress/config/install/.htaccess wordpress/web/.htaccess
        echo "[\e[92mINSTALL\e[0m] - Prepare .htaccess done ! \e[0m";
        cp wordpress/config/install/wp-config.php wordpress/web/wp-config.php
        echo "[\e[92mINSTALL\e[0m] - Prepare wp-config.php done ! \e[0m";
        rm -rf wordpress/web/wp-content
        rm -rf wordpress/web/wp-sites/wp-content
        echo "[\e[92mINSTALL\e[0m] - Delete all wordpress base themes ! \e[0m";
        mv wordpress/config/install/modules/bushido-wp wordpress/web/wp-ressources/modules/bushido-wp-${MAIN_APP_NAME}
        mv wordpress/config/install/themes/wordpress-bushido wordpress/web/wp-sites/themes/wordpress-bushido-${MAIN_APP_NAME}
        echo "[\e[92mINSTALL\e[0m] - Install base project custom module and theme bushido-wp done ! \e[0m";
        mkdir wordpress/web/wp-media
        chmod 777 wordpress/web/wp-media
        echo "[\e[92mINSTALL\e[0m] - Wordpress Media Directory created and rights done ! \e[0m";
        cd wordpress/web && wp core install --url="https://${MAIN_APP_NAME}-local.koomma.io" --title="Wordpress" --admin_user="bushido" --admin_password="koomma" --admin_email="wordpress.adm@koomma.fr"
        echo "[\e[92mINSTALL\e[0m] - Wordpress WP CLI Installation done ! \e[0m";
        wp plugin activate query-monitor;
        echo "[\e[92mINSTALL\e[0m] - Wordpress Plugin : activate query-monitor done ! \e[0m";
        wp plugin activate user-switching
        echo "[\e[92mINSTALL\e[0m] - Wordpress Plugin : activate user-switching done ! \e[0m";
        wp plugin activate wp-crontrol
        echo "[\e[92mINSTALL\e[0m] - Wordpress Plugin : activate wp-crontrol done ! \e[0m";
        wp plugin activate wpbushido-helper
        echo "[\e[92mINSTALL\e[0m] - Wordpress Plugin : activate wpbushido-helper done ! \e[0m";
        wp plugin activate bushido-wp-${MAIN_APP_NAME}
        echo "[\e[92mINSTALL\e[0m] - Wordpress Plugin : activate bushido-wp done ! \e[0m";
        wp plugin activate advanced-custom-fields-pro
        echo "[\e[92mINSTALL\e[0m] - Wordpress Plugin : activate advanced-custom-fields-pro done ! \e[0m";
        wp language core install fr_FR
        echo "[\e[92mINSTALL\e[0m] - Wordpress Lang : install fr_FR done ! \e[0m";
        wp site switch-language fr_FR
        echo "[\e[92mINSTALL\e[0m] - Wordpress Lang : activate fr_FR done ! \e[0m";
        wp theme activate wordpress-bushido-${MAIN_APP_NAME}
        echo "[\e[92mINSTALL\e[0m] - Wordpress Theme : activate wordpress-bushido-base child theme ! \e[0m";
        rm -rf .git
        echo "[\e[92mINSTALL\e[0m] - Clean Git from Wordpress ! \e[0m";
        break;;
        [Nn]* ) exit;;
        * ) echo "[\e[91mERROR\e[0m] - Please answer y or n.";;
    esac
done