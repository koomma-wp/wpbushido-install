<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPBushidoProject
 */

namespace WPBushidoProject\Page;

class Pagehomepage extends Page
{
    public function process()
    {
        parent::process();
        $this->renderTpl();
    }
}