<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPBushidoProject
 */

namespace WPBushidoProject\Page;

class Page extends \WPBushidoCore\Page\Page
{
    public function __construct(array $context)
    {
        parent::__construct($context);
    }
}