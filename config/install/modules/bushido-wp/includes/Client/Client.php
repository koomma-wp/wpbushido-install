<?php
/**
 * WPLeooProject Plugin
 *
 * @package WPLeooProject
 */

namespace WPBushidoProject\Client;

use WPBushidoProject\Page\Page;

class Client extends \WPBushidoCore\Client\Client
{

    public function __construct() {
        parent::__construct();
    }

    public function addToContext($context) {
        $context = parent::addToContext($context);
        if (!empty($context['page_template'])) {
            $namePageClass = 'WPLeooProject\\Page\\Page' . str_replace(array('-', '_'), array('', ''), ucfirst($context['page_template']));
            $namePageParentClass = '\\WPBushidoCore\\Page\\Page' . str_replace(array('-', '_'), array('', ''), ucfirst($context['page_template']));
            if (class_exists($namePageClass)) {
                $page = new $namePageClass($context);
                $page->process();
                $context = $page->getContext();
            } else if (class_exists($namePageParentClass)) {
                $page = new $namePageParentClass($context);
                $page->process();
                $context = $page->getContext();
            }
        }
        return $context;
    }
}
