<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPLeooProject
 */

namespace WPBushidoProject\Security;

class Settings
{
    /**
     * Setup Security settings methods
     *
     * @return void
     */
    public static function setup()
    {
        add_action('admin_init', array(self::class, 'disableCommentsSupport'));
        add_filter('comments_open', array(self::class, 'disableCommentsStatus'), 20, 2);
        add_filter('pings_open', array(self::class, 'disableCommentsStatus'), 20, 2);
        add_filter('comments_array', array(self::class, 'disableCommentsHideExistingComments'), 10, 2);
        add_action('admin_menu', array(self::class, 'removeAdminCommentsMenu'));
        add_action('admin_init', array(self::class, 'redirectAdminCommentsMenu'));
        add_action('admin_init', array(self::class, 'disableDashboardMetabox'));
        add_action('init', array(self::class, 'disableCommentsAdminBar'));
    }

    /**
     * Disable comments support in admin
     *
     * @return void
     */
    public static function disableCommentsSupport()
    {
        $post_types = get_post_types();
        foreach ($post_types as $post_type) {
            if(post_type_supports($post_type, 'comments')) {
                remove_post_type_support($post_type, 'comments');
                remove_post_type_support($post_type, 'trackbacks');
            }
        }
    }

    /**
     * Disable comments status
     *
     * @return boolean
     */
    public static function disableCommentsStatus()
    {
        return false;
    }

    /**
     * Disable comments status
     *
     * @return array
     */
    public static function disableCommentsHideExistingComments()
    {
        $comments = array();
        return $comments;
    }

    /**
     * Remove Admin Comments Menu
     *
     * @return void
     */
    public static function removeAdminCommentsMenu()
    {
        remove_menu_page('edit-comments.php');
    }

    /**
     * Redirect comment admin page
     *
     * @return void
     */
    public static function redirectAdminCommentsMenu()
    {
        global $pagenow;
        if ($pagenow === 'edit-comments.php') {
            wp_redirect(admin_url()); exit;
        }
    }

    /**
     * Remove Dashboard metabox
     *
     * @return void
     */
    public static function disableDashboardMetabox()
    {
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    }

    /**
     * Disable Comments Admin Bar
     *
     * @return void
     */
    public static function disableCommentsAdminBar()
    {
        if (is_admin_bar_showing()) {
            remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }
    }
}
