<?php
/**
 * Created by PhpStorm.
 * User: Philippe AUTIER
 * Date: 04/01/19
 * Time: 14:49
 */

require_once __DIR__ . '/../vendor/autoload.php';

if (!defined('WP_INSTALL_CONFIG')) {
    define('WP_INSTALL_CONFIG', require __DIR__ . '/../config/vars.php');
}

if (PHP_SAPI === 'cli') {
    $_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.0';
    $_SERVER['HTTP_USER_AGENT'] = '';
    $_SERVER['REQUEST_METHOD']  = 'GET';
    $_SERVER['REMOTE_ADDR']     = '127.0.0.1';
    $_SERVER['SERVER_NAME']     = WP_INSTALL_CONFIG['WEB_DOMAIN'];
    $_SERVER['HTTP_HOST']       = WP_INSTALL_CONFIG['WEB_DOMAIN'];
}

require_once __DIR__ . '/../config/environments/' . WP_INSTALL_CONFIG['ENVIRONEMENT'] . '.php';
require_once __DIR__ . '/../config/application.php';
require_once __DIR__ . '/../config/salt-keys.php';

require_once ABSPATH . 'wp-settings.php';