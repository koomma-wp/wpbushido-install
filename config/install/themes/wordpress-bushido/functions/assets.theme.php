<?php

if ( ! function_exists( 'wptm_scripts' ) ) {
    /**
     * Load theme's JavaScript and CSS sources.
     */
    function wptm_scripts() {
        // Get the theme data.
        $the_theme     = wp_get_theme();
        $theme_version = $the_theme->get( 'Version' );
        $css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
        wp_enqueue_style( 'understrap-styles', get_template_directory_uri() . '/css/theme.min.css', array(), $css_version );
        wp_enqueue_script( 'jquery' );
        $js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/theme.min.js' );
        wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $js_version, true );
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
    }
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'wptm_scripts' );

if ( ! function_exists( 'wptm_child_scripts' ) ) {
    /**
     * Load theme's JavaScript and CSS sources.
     */
    function wptm_child_scripts() {
        // Get the theme data.
        $the_theme     = wp_get_theme();
        $theme_version = $the_theme->get( 'Version' );
        $css_version = $theme_version . '.' . filemtime( get_stylesheet_directory() . '/css/child-theme.min.css' );
        wp_enqueue_style( 'understrap-child-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $css_version );
        $js_version = $theme_version . '.' . filemtime( get_stylesheet_directory() . '/js/child-theme.min.js' );
        wp_enqueue_script( 'understrap-child-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $js_version, true );
    }
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'wptm_child_scripts' );