#!/bin/bash
echo "\e[36mStart Wordpress BUSHIDO Update\e[0m";
echo "[\e[92mUPDATE\e[0m] - Start Wordpress BUSHIDO\e[0m";
cp wordpress/config/install/.htaccess wordpress/web/.htaccess
echo "[\e[92mUPDATE\e[0m] - Prepare .htaccess done ! \e[0m";
cp wordpress/config/install/wp-config.php wordpress/web/wp-config.php
echo "[\e[92mUPDATE\e[0m] - Prepare wp-config.php done ! \e[0m";
rm -rf wordpress/web/wp-content
rm -rf wordpress/web/wp-sites/wp-content
echo "[\e[92mUPDATE\e[0m] - Delete all wordpress base themes ! \e[0m";
mkdir -p wordpress/web/wp-media
chmod 777 wordpress/web/wp-media
rm -rf wordpress/web/.git
echo "[\e[92mUPDATE\e[0m] - Clean Git from Wordpress ! \e[0m";