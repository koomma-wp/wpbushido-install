<?php

define('WP_INSTALL_CONFIG_ADD', array (
    'ENVIRONEMENT' => $_ENV["STACK_ENVIRONMENT"],
    'DEV_MAIL' => 'wordpress.adm@koomma.fr',
    'DB_HOST' => 'mysql',
    'DB_NAME' => $_ENV["MYSQL_DB_NAME"],
    'DB_USER' => $_ENV["MYSQL_DB_USER"],
    'DB_PASSWORD' => $_ENV["MYSQL_DB_PASSWORD"],
    'WEB_SCHEME' => 'https',
    'WEB_DOMAIN' => $_ENV["MAIN_APP_NAME"].'-local.koomma.io',
    'WEB_PATH' => '',
));

/* DEBUG */
ini_set('display_errors', 1);
define('WP_DEBUG', true);
define('WP_DEBUG_DISPLAY', true);
define('SCRIPT_DEBUG', true);
define('SAVEQUERIES', PHP_SAPI != 'cli');

/* MEMORY */
define('WP_MEMORY_LIMIT', '128M');

/* SECURITY */
define('FORCE_SSL_ADMIN', true);

/* AUTOSAVE, REVISIONS, TRASH */
define('AUTOSAVE_INTERVAL', '300');
define('WP_POST_REVISIONS', false);
define('MEDIA_TRASH', false);
define('EMPTY_TRASH_DAYS', '50');

/* HIDE ACF MENU */
define('ACF_LITE', false);

/* WP-CRON */
define('DISABLE_WP_CRON', false);
define('ALTERNATE_WP_CRON', PHP_SAPI != 'cli');
define('WP_CRON_LOCK_TIMEOUT', 60);