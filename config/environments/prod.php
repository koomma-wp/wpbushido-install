<?php

define('WP_INSTALL_CONFIG_ADD', array (
    'ENVIRONEMENT' => $_ENV["STACK_ENVIRONMENT"],
    'DEV_MAIL' => 'wordpress.adm@koomma.fr',
    'DB_HOST' => 'mysql',
    'DB_NAME' => $_ENV["MYSQL_DB_NAME"],
    'DB_USER' => $_ENV["MYSQL_DB_USER"],
    'DB_PASSWORD' => $_ENV["MYSQL_DB_PASSWORD"],
    'WEB_SCHEME' => 'https',
    'WEB_DOMAIN' => $_ENV["MAIN_APP_NAME"].'-prod.koomma.io',
    'WEB_PATH' => '',
));

/* DEBUG */
ini_set('display_errors', 0);
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);
define('SCRIPT_DEBUG', false);
define('SAVEQUERIES', false);

/* MEMORY */
define('WP_MEMORY_LIMIT', '256M');

/* SECURITY */
define('FORCE_SSL_ADMIN', true);

/* AUTOSAVE, REVISIONS, TRASH */
define('AUTOSAVE_INTERVAL', '300');
define('WP_POST_REVISIONS', 10);
define('MEDIA_TRASH', true);
define('EMPTY_TRASH_DAYS', '50');

/* HIDE ACF MENU */
define('ACF_LITE', true);

/* WP-CRON */
define('DISABLE_WP_CRON', false);
define('ALTERNATE_WP_CRON', false);
define('WP_CRON_LOCK_TIMEOUT', 60);