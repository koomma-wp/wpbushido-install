<?php
/**
 * Created by PhpStorm.
 * User: Philippe AUTIER
 * Date: 04/01/19
 * Time: 15:07
 */

/* ENVIRONEMENT */
define('WP_ENV', WP_INSTALL_CONFIG['ENVIRONEMENT']);

/* DATABASE */
define('DB_NAME', WP_INSTALL_CONFIG_ADD['DB_NAME']);
define('DB_USER', WP_INSTALL_CONFIG_ADD['DB_USER']);
define('DB_PASSWORD', WP_INSTALL_CONFIG_ADD['DB_PASSWORD']);
define('DB_HOST', WP_INSTALL_CONFIG_ADD['DB_HOST']);
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
$table_prefix = $_ENV["MYSQL_DB_WP_PREFIX"];

/* DIRECTORIES */
define('ROOT_DIR', dirname(__DIR__));
define('WP_SCHEME', WP_INSTALL_CONFIG_ADD['WEB_SCHEME']);
define('WP_DOMAIN', WP_INSTALL_CONFIG_ADD['WEB_DOMAIN']);
define('WP_PATH', WP_INSTALL_CONFIG_ADD['WEB_PATH']);
define('WP_HOME', WP_SCHEME . '://' . WP_DOMAIN . WP_PATH);
define('WEBROOT_DIR', ROOT_DIR . '/web');
define('WP_SITEURL', WP_HOME);
define('WP_PLUGIN_DIR', WEBROOT_DIR . '/wp-ressources/modules');
define('WP_PLUGIN_URL', WP_HOME . '/wp-ressources/modules');
define('WPMU_PLUGIN_DIR', WEBROOT_DIR . '/wp-ressources/mu-modules');
define('WPMU_PLUGIN_URL', WP_HOME . '/wp-ressources/mu-modules');
define('WP_UPLOADS_DIR', WEBROOT_DIR . '/wp-media');
define('WP_UPLOADS_URL', WP_HOME . '/wp-media');
define('UPLOADS', 'wp-media/uploads');
define('WP_LANG_DIR', WEBROOT_DIR . '/wp-ressources/languages');
define('WP_TIMBER_CACHE_DIR', WEBROOT_DIR .'/wp-media/cache');
define('WP_CONTENT_DIR', WEBROOT_DIR.'/wp-sites');
define('WP_CONTENT_URL', WP_HOME.'/wp-sites');

/* HTTPS */
if ('https' === WP_SCHEME) {
    define('FORCE_SSL_ADMIN', true);
    if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $_SERVER['HTTPS'] = 'on';
    }
}
define('WP_IS_HTTPS', ('https' === WP_SCHEME));

/* THEME */
#define('WP_DEFAULT_THEME', 'wordpress-leoo-base-theme');

/* UPDATES */
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_AUTO_UPDATE_CORE', false);

/* SECURITY */
define('DISALLOW_FILE_MODS', true);
define('DISALLOW_FILE_EDIT', true);

/* PERFORMANCES */
define('COMPRESS_CSS', true);
define('COMPRESS_SCRIPTS', true);
define('CONCATENATE_SCRIPTS', true);
define('ENFORCE_GZIP', true);

/* WONOLOG */
define('WP_DEBUG_LOG', false);